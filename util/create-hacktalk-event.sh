#!/bin/bash

# Check if all needed programs are installed.
check_preconditions() {
  if ! command -v uuidgen &> /dev/null
  then
      echo "Necessary command 'uuidgen' could not be found. Exiting."
      exit
  fi
}

# Print arguments for this script.
print_arguments() {
	echo "BEGIN - Time of event begin in the format yyyy-mm-dd hh:mm:ss, for example 2022-05-03 17:00:00."
	echo "END - Time of event end in the format yyyy-mm-dd hh:mm:ss, for example 2022-05-03 19:00:00."
	echo "SUMMARY - String that is used as a title. This is prefixed with 'HackTalk: '."
	echo "DESCRIPTION - String that is used as the description of the event. Please make sure linebreaks are masked as '\\\\\n'."
}

# Print help for this script.
# $1 all of the arguments passed to this script
handle_help() {
  while getopts ":h" option; do
     case $option in
        h)
           echo "This will create an ICS calendar file for a HackTalk event."
           echo
           echo "Usage:"
           echo "./create-hacktalk-event BEGIN END SUMMARY DESCRIPTION"
           echo
           print_arguments
           exit;;
         *)
           exit;;
     esac
  done
}

# Check if the right number of arguments has been passed to the script.
# $1 all of the arguments passed to this script
handle_too_few_arguments() {
  if [ $# -lt 4 ]
  then
    # Output error to stderr
    print_too_few_arguments >&2
    exit
  fi
}

# Print an error message if too few arguments have been passed to the script.
print_too_few_arguments() {
  echo "Please provide the following arguments:"
  echo
  print_arguments
}

# Create the ICS file.
# $1 all of the arguments passed to this script
create_ics_file() {
  UUID=$(uuidgen)
  DTSTAMP=$(date --utc +%Y%m%dT%H%M%S)
  BEGIN=$(date -d "$1" +"%Y%m%dT%H%M%S")
  END=$(date -d "$2" +"%Y%m%dT%H%M%S")
  SUMMARY="HackTalk: $3"
  DESCRIPTION="$4"

  FILENAME="../dist/hacktalk_$BEGIN.ics"

  cat template.ics | \
    sed "s/UUID:YOUR_RANDOM_UUID/UUID:$UUID/" | \
    sed "s/DTSTAMP:CREATION_TIME/DTSTAMP:$DTSTAMP/" | \
    sed "s/DTSTART:START_TIME/DTSTART;TZID=Europe\/Berlin:$BEGIN/" | \
    sed "s/DTEND:END_TIME/DTEND;TZID=Europe\/Berlin:$END/" | \
    sed "s/SUMMARY:TITLE_AND_SUBTITLE/SUMMARY:$SUMMARY/" | \
    sed "s/DESCRIPTION:DESCRIPTION_TEXT/DESCRIPTION:$DESCRIPTION/" \
    > "$FILENAME"

  echo
  echo "File $FILENAME created."
}

main() {

  check_preconditions
  handle_help "$@"
  handle_too_few_arguments "$@"

  create_ics_file "$@"
}

main "$@"